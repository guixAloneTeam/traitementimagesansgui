
public class Vector2 {
	double x;
	double y;
	
	public Vector2(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	public double norm(){
		return Math.sqrt((this.x * this.x) + (this.y * this.y));
	}
	
	
}
