import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class MainApplication {
	
	
	public static void main(String[] args) {
		BufferedImage img = null;
		
		//lecture image
		try 
		{
			//img = ImageIO.read(new File("images_test/test.png"));
			//img = ImageIO.read(new File("images_test/building.png"));
			img = ImageIO.read(new File("images_test/tools.png"));
			//img = ImageIO.read(new File("images_test/hand.jpg"));
		} 
		catch (IOException e) 
		{
		    e.printStackTrace();
		}
		
		Image image = new Image(img);
		//image.convertFromBuffered(); //int�gr� au constructeur
		
		/****** SOBEL ******/
	/*	image.luminance();
		Sobel sob = new Sobel();
		//image = sob.applyBrut(image);  //application direct et brut pour Sobel
		image = sob.apply(image); //application avec prise en compte taille filtre
		BufferedImage tmp = image.convertFromImage(); */
		
		/****** Dilatation ******/
		/*image.luminance();
		image.seuillage();
		Dilatation dil = new Dilatation();
		image = dil.dilatationMultiple(image,10);
		BufferedImage tmp = image.convertFromImage(); */
		
		/****** Erosion ******/
		image.luminance();
		image.seuillage();
		Erosion ero = new Erosion();
		//image = ero.apply(image);
		image = ero.erosionMultiple(image,5);  
		BufferedImage tmp = image.convertFromImage(); 
		
		
		//enregistrement
		File outputfile = new File("result.png");
		try
		{
			ImageIO.write(tmp, "png", outputfile);
		}
		catch (Exception e)
		{
			System.out.println(e.toString());
		}
		
		System.out.println("Complete.");
		
	}

}
