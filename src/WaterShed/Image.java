package WaterShed;
import java.awt.Color;
import java.awt.image.BufferedImage;

public class Image {
	BufferedImage original;
	double[][][] img;
	
	public Image(){
		
	}
	
	public Image(BufferedImage img){
		this.img = new double[img.getWidth()][img.getHeight()][3];
		this.original = img;
		for(int i = 0; i < img.getWidth() ; i ++){
			for(int j=0 ; j< img.getHeight(); j ++){
				int tmp = img.getRGB(i, j);
				// de 0 a 255
				double b = tmp & 0xff;
				double g = (tmp & 0xff00) >> 8;
				double r = (tmp & 0xff0000) >> 16;
				
				this.img[i][j][0] = r;
				this.img[i][j][1] = g;
				this.img[i][j][2] = b;
			}
		}
	}
	
	public Image(Image img){
		this.original = img.original;
		this.img = img.img;
	}
	
	public Image(int a, int b){  //creer une image vide avec juste une taille
		this.img = new double[a][b][3];
	}
	
	
	public void convertFromBuffered(){
		this.img = new double[this.original.getWidth()][this.original.getHeight()][3];
		for(int i = 0; i < this.original.getWidth() ; i ++){
			for(int j=0 ; j< this.original.getHeight(); j ++){
				int tmp = this.original.getRGB(i, j);
				// de 0 a 255
				double b = tmp & 0xff;
				double g = (tmp & 0xff00) >> 8;
				double r = (tmp & 0xff0000) >> 16;
				
				this.img[i][j][0] = r;
				this.img[i][j][1] = g;
				this.img[i][j][2] = b;
			}
		}
				
		
	}
	
	public BufferedImage convertFromImage(){
		BufferedImage tmp = new BufferedImage(this.original.getWidth(), this.original.getHeight(), BufferedImage.TYPE_INT_RGB);
		for(int i=0 ; i<this.original.getWidth(); i++){
			for(int j=0 ; j<this.original.getHeight(); j++){
				int tmpColor = new Color((int) this.img[i][j][0] , (int)this.img[i][j][1] ,(int) this.img[i][j][2]).getRGB();
				tmp.setRGB(i, j, tmpColor);
			}
		}
		return tmp;
	}
	
	public void luminance(){
		// i = 0.2126 x R + 0.7152 * G + 0.0722 * B -> mettre cette valeur dans rouge, vert bleu pour convertir en luminance 
		for(int i =0 ; i<this.getWidth(); i++){
			for(int j=0 ; j<this.getHeight() ; j++){
				double moyenneRGB = ((this.img[i][j][0]*0.2126) + (this.img[i][j][1] * 0.7152) + (this.img[i][j][2]*0.0722) ) ;
				
				for(int k=0; k<3 ; k++)this.img[i][j][k]=moyenneRGB;
			}
		}
		
	}
	
	public void seuillage(){
		for(int i =0 ; i<this.getWidth(); i++){
			for(int j=0 ; j<this.getHeight() ; j++){
				if(this.img[i][j][0]>128){
					this.img[i][j][0] = 255;
					this.img[i][j][1] = 255;
					this.img[i][j][2] = 255;
				}
				else
				{
					if(this.img[i][j][0]<128){
						this.img[i][j][0] = 0;
						this.img[i][j][1] = 0;
						this.img[i][j][2] = 0;
					}
				}
			}
		}
	}
	
	public int getHeight(){
		return this.original.getHeight(); 
	}
	
	public int getWidth(){
		return this.original.getWidth(); 
	}
	

}

